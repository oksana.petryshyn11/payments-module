import './App.css';
import { Route, Switch, useHistory } from 'react-router-dom';
import ListRequestComponent from './components/ListRequestComponent';
import AddRequestComponent from './components/AddRequestComponent';
import { Header } from './components/Header';
import { useSelector } from 'react-redux';
import { NavPanel } from './components/NavPanel';
import { navPages } from './components/utils/pages';
import { CreateRequestPage } from './components/pages/createRequestPage';
import { CreateRequest } from './components/pages/createRequest';
import { SignPage } from './components/pages/SignPage';
import { selectRoles, selectUsername } from './components/store/auth/reducer';
import { useEffect } from 'react';
import { Card } from './components/Card';
import { AdminRequest } from './components/pages/adminPages/adminRequest';
import { AdminEditRequest } from './components/pages/adminPages/adminEditRequest';
import { AddProgram } from './components/pages/adminPages/addProgram';
import { AccRequest } from './components/pages/adminPages/accRequests';
import { House } from './components/expages/House';
import { Health } from './components/expages/health';
import { Finans } from './components/expages/finans';
import { Pereprof } from './components/expages/pereprof';
import { Work } from './components/expages/work';
import { PsyhHelp } from './components/expages/psyhologHelp';
import { RechHelp } from './components/expages/rechHelp';


export const Content = () => {
    const isLogin = useSelector(selectUsername);
    const userRole = useSelector(selectRoles)[0];
    const history = useHistory();

    useEffect(() => {
        if (!isLogin) {
            history.push('/login');
        }
    }, [isLogin, history]);

    return (
        <div className='page-content'>
            {isLogin && <Header isLogin={isLogin} userName={userRole !== 'ROLE_USER' ? userRole.split('_')[1] : isLogin} />}
            <div className='content-main'>
                {isLogin && <NavPanel pages={navPages}></NavPanel>}
                <div className='content-holder'>
                    <Switch>
                        <Route exact path="/home" component={Card}></Route>
                        <Route exact path="/login" component={SignPage}></Route>
                        <Route path="/state-payments/edit-request/:requestId" component={AdminEditRequest}></Route>
                        <Route path="/state-payments/add-program" component={AddProgram}></Route>
                        <Route path="/state-payments/create-request" component={CreateRequest}></Route>

                        {userRole === 'ROLE_ACCOUNTANT' &&
                            <Route path="/state-payments" component={AccRequest}></Route>}
                        {userRole !== 'ROLE_USER'
                            ? <Route path="/state-payments" component={AdminRequest}></Route>
                            : <Route path="/state-payments" component={CreateRequestPage}></Route>}

                        <Route path="/dwelling" component={House}></Route>
                        <Route path="/medicine-&-education" component={Health}></Route>
                        <Route path="/assistance-to-organizations" component={Finans}></Route>
                        <Route path="/repurposing" component={Pereprof}></Route>
                        <Route path="/work" component={Work}></Route>
                        <Route path="/psychological-help" component={PsyhHelp}></Route>
                        <Route path="/material-assistance" component={RechHelp}></Route>
                        <Route path="/requests" component={ListRequestComponent}></Route>
                        <Route path="/add-request" component={AddRequestComponent}></Route>
                        <Route path="/edit-request/:id" component={AddRequestComponent}></Route>
                    </Switch>
                </div>
            </div>
        </div>

    );
}
