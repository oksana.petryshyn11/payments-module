import axios from 'axios'

const REQUEST_BASE_REST_API_URL = 'http://localhost:8080/api/v1/requests';

class RequestService {

    getAllRequests(token) {
        return axios.get(REQUEST_BASE_REST_API_URL, { headers: { Authorization: `Bearer ${token}` } })
    }

    createRequest(request, token) {
        return axios.post(REQUEST_BASE_REST_API_URL, request, { headers: { Authorization: `Bearer ${token}` } })
    }

    getRequestById(requestId, token) {
        return axios.get(REQUEST_BASE_REST_API_URL + '/' + requestId, { headers: { Authorization: `Bearer ${token}` } });
    }

    updateRequest(requestId, request) {
        return axios.put(REQUEST_BASE_REST_API_URL + '/' + requestId, request);
    }

    deleteRequest(requestId, token) {
        return axios.put(REQUEST_BASE_REST_API_URL + '/delete/' + requestId, {}, { headers: { Authorization: `Bearer ${token}` } });
    }

    updateRequestWithStatus(requestId, status, token) {
        return axios.put(REQUEST_BASE_REST_API_URL + `/change_status/${requestId}?status=${status}`, {}, { headers: { Authorization: `Bearer ${token}` } });
    }
}

export default new RequestService();