import axios from 'axios'

const PROGRAM_BASE_REST_API_URL = 'http://localhost:8080/api/v1/payment_program';

class PaymentProgramService {

    getAllPaymentPrograms(token) {
        return axios.get(PROGRAM_BASE_REST_API_URL, {headers: { Authorization: `Bearer ${token}`}})
    }

    addPaymentProgram(body,token) {
        return axios.post(PROGRAM_BASE_REST_API_URL,body, {headers: { Authorization: `Bearer ${token}`}})
    }
}

export default new PaymentProgramService();