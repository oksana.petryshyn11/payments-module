import axios from 'axios'

const REQUEST_BASE_REST_API_URL = 'http://localhost:8080/api/v1/auth';

class SignService {

    signIn(request) {
        return axios.post(REQUEST_BASE_REST_API_URL + '/signin', request)
    }

    signUp(request) {
        return axios.post(REQUEST_BASE_REST_API_URL + '/signup', request)
    }
}

export default new SignService();