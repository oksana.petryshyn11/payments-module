import './RequestPageHeader.styles.css';
import React from 'react';
import { Link } from 'react-router-dom'
import { ProgramsInfo } from './programsInfo';

export const RequestPageHeader = ({
  onCreateClick,
  onProgramsClick
}) => {

    return (
        <div className='page-header'>
            <h3>Мої заяви</h3>
            <div className='buttons-controller-2'>
            <ProgramsInfo/>
            <Link to='/state-payments/create-request'><button className='btn-main' onClick={onCreateClick}>Створити заяву</button></Link>
            </div>
        </div>
    )
}
