import './programsInfo.styles.css';
import Modal from 'react-modal';
import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { selectToken } from './store/auth/index';
import PaymentProgram from '../services/PaymentProgram';

export const ProgramsInfo = () => {
    const [isOpen, setIsOpen] = useState(false);
    const token = useSelector(selectToken);
    const [programs, setPrograms] = useState([]);

    useEffect(() => {

        getAllPrograms();
    }, [])

    const getAllPrograms = () => {
        PaymentProgram.getAllPaymentPrograms(token).then((response) => {
            setPrograms(response.data);
            console.log(response.data);
        }).catch(error => {
            console.log(error);
        })
    }

    return <>
        <button className='btn-main-5' onClick={() => setIsOpen(true)}>Інформація про програми</button>
        <Modal
            className='modal-my'
            isOpen={isOpen}
            onRequestClose={() => setIsOpen(false)}
            contentLabel="Інформація про програми"
        >
            <h2>Інформація про програми</h2>
            <hr />
            <div className='content-modal-my'>
            {programs.map((item) => <div key={item.name}>
                <h3>{item.name}</h3>
                <p>{item.description}</p>
                <p>Розмір виплати: {item.amount} грн</p>
                <hr />
            </div>)}
            </div>
            <button className='btn-main-5' onClick={()=>setIsOpen(false)}>Close</button>
        </Modal>
    </>
}