import { configureStore } from '@reduxjs/toolkit'; 
import { setupListeners } from '@reduxjs/toolkit/query'; 
import rootReducer from './rootReducer'; 
 
const store = configureStore({ 
  reducer: rootReducer, 
}); 
 
export default store; 
 
setupListeners(store.dispatch);