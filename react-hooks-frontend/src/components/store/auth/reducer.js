
import { createSlice } from '@reduxjs/toolkit';

export const initialState = {
    roles: [
        ""
    ],
    token: "",
    id: 0,
    username: "",
    email: ""
}

const authSlice = createSlice({
    name: 'Auth',
    initialState,
    reducers: {
        setAuth: (state, action) => {
            state.username = action.payload.username;
            state.roles = action.payload.roles;
            state.token = action.payload.token;
            state.id = action.payload.id;
            state.username = action.payload.username;
            state.email = action.payload.email;
        },
    },
});

export const selectToken = (state) =>
    state?.auth?.token;

export const selectUsername = (state) =>
    state?.auth?.username;

    export const selectRoles = (state) =>
    state?.auth?.roles;

export const { setAuth } = authSlice.actions;

export const auth = authSlice.reducer;