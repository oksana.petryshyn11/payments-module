import './NavPanel.styles.css';
import React from 'react'
import { NavLink, useLocation } from 'react-router-dom'

export const NavPanel = ({
    pages
}) => {
    const location = useLocation();

    return (
        <div className='root'>
            <ul className="nav-panel-list">
                {pages.map(({ path, title, icon }) => <li key={path}>
                    <NavLink
                        to={path}
                        className={`${location?.pathname.startsWith(path) && 'active'}`}>
                        <div className='nav-item-panel'>
                            {/* <div><img src={icon} alt="*"/></div> */}
                            <p>{title}</p>
                        </div>
                    </NavLink>

                </li>)}
            </ul>
        </div>
    )
}
