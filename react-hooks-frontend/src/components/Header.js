import { useHistory } from 'react-router-dom';
import './Header.styles.css';
import { useDispatch } from 'react-redux';
import { setAuth } from '../components/store/auth/index';
import logoHelp from './logoHelp.svg';
import logoUser from './logoUser.svg';

export const Header = ({
    isLogin,
    userName
}) => {
    const history = useHistory();
    const dispatch = useDispatch();

    const onExit = () => {
        dispatch(setAuth({ roles: [
            ""
        ],
        token: "",
        id: 0,
        username: "",
        email: ""}));
        history.push('/login');
    }

    return <div className='header'>
        <header className='header-root'>
            <div className='logo-content'>
                <img src={logoHelp} alt="Logo" />
                <p className='logo-text'>Є-Допомога</p>
            </div>
            {isLogin
                ? <div className='user-controller'>
                    <img src={logoUser} alt="Logo" />
                    <p className='user-text'>{userName}</p>
                    <button onClick={onExit} className='exit-btn'>Вихід</button>
                </div>
                : <div className='buttons-controller'>
                    <button>Log in</button>
                    <button>Register</button>
                </div>
            }
        </header>
    </div>
}