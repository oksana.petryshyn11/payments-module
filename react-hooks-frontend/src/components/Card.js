import './Card.styles.css';
import cover from '../imgs/cover.png';
import cover1 from '../imgs/cover1.png';
import cover2 from '../imgs/cover2.png';
import cover3 from '../imgs/cover3.png';
import cover4 from '../imgs/cover4.png';
import cover5 from '../imgs/cover5.png';
import cover6 from '../imgs/cover6.png';
import cover7 from '../imgs/cover7.png';
import { Link } from 'react-router-dom';

const cards = [{
    image: cover7,
    title: 'Формування пропозицій житла',
    text: 'Пошук актуальних пропозицій житла для вимушено переміщених осіб.',
    path: '/dwelling'
},
{
    image: cover6,
    path: '/medicine-&-education',
    title: 'Медичних та освітніх заклади',
    text: 'Надання медичної допомоги  постраждалим особам.Можливість відвідування освітніх закладів.'
},
{
    image: cover5,
    path: '/assistance-to-organizations',
    title: 'Розподілення фінансової допомоги організацій',
    text: 'Управління коштами, отриманими від організацій.'
},
{
    image: cover4,
    path: '/repurposing',
    title: 'Перепрофілювання та працевлаштування',
    text: 'Допомоги постраждалим особам у вигляді надавання безкоштовного перепрофілювання.'
},
{
    image: cover3,
    path: '/work',
    title: 'Пошук роботи',
    text: 'Пошук роботи постраждалим чи вимушено переміщеним особам.'
},
{
    image: cover2,
    path: '/psychological-help',
    title: 'Надання психологічних послуг',
    text: 'Надання безкоштовної психологічної допомоги постраждалим внаслідок війни.'
},
{
    image: cover1,
    path: '/state-payments',
    title: 'Державні виплати',
    text: 'Надання матеріальної допомоги людям, що втратили роботу внаслідок війни.'
},
{
    image: cover,
    path: '/material-assistance',
    title: 'Формування речової допомоги',
    text: 'Надання гуманітарної допомоги постраждалим сім’ям.'
}]

export const Card = () => {

    return <>
        <div className='page-header'>
            <h3>Оберіть категорію</h3>
        </div>
        <div className='page-cards'>{cards.map(({ image,
            title,
            text,
            path }) => <div key={path} className='card'>
                <img className='img-card' src={image} alt='card' />
                <div className='card-content'>
                    <Link className='link-stk' to={path}><h5>{title}</h5></Link>
                    <p>{text}</p>
                </div>
            </div>
        )}</div>
    </>
}