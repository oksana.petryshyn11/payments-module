import './createRequest.styles.css';
import React, { useEffect, useState } from 'react'
import RequestService from '../../services/RequestService';
import { useSelector } from 'react-redux';
import ArrowLeft from './ArrowLeft.svg';
import { Link } from 'react-router-dom';
import { useHistory } from "react-router-dom";
import Select from 'react-select';
import { selectToken } from '../store/auth';
import PaymentProgram from '../../services/PaymentProgram';
import { ProgramsInfo } from '../programsInfo';

export const defaultValues = {
    paymentProgramName: null,
    description: ''
}

export const CreateRequest = () => {
    const history = useHistory();
    const token = useSelector(selectToken);

    const [state, setState] = useState(defaultValues);

    const [programs, setPrograms] = useState([]);

    useEffect(() => {

        getAllPrograms();
    }, [])

    const getAllPrograms = () => {
        PaymentProgram.getAllPaymentPrograms(token).then((response) => {
            setPrograms(response.data.map((program) => ({
                label: program.name,
                value: program.id
            })))
            console.log(response.data);
        }).catch(error => {
            console.log(error);
        })
    }

    const onChange = (newValue, key) => setState((prev) => ({
        ...prev,
        [key]: newValue,
    }));

    const saveOrUpdateRequest = (e) => {
        e.preventDefault();

        const request = {
            programName: state.paymentProgramName.label,
            description: state.description
        }

        RequestService.createRequest(request, token).then((response) => {

            console.log(response.data)
            setState(defaultValues);
            history.push('/state-payments')

        }).catch(error => {
            console.log(error)
        })
    }

    return <>
        <div className='header-rqst'>
            <p className='text-request'><span>Мої заяви / </span>Створити заяву</p>
            <div className='back'>
                <Link to='/state-payments'>
                    <img
                        className='arrow-left'
                        src={ArrowLeft} alt="Arrow" /></Link>

                <h3>Створити заяву</h3>
            </div>

            <ProgramsInfo/>
        </div>


        <div className='create-request'>
            <div className='content-request'>
                <div className='field-row'>
                    <p>Категорія виплати :</p>
                    <Select
                        value={state.paymentProgramName}
                        onChange={(value) => onChange(value, 'paymentProgramName')}
                        options={programs}
                        className='selector'
                    />
                </div>
                <div className='field-row'>
                    <p>Опис:</p>
                    <textarea
                        className='descr'
                        value={state.description}
                        onChange={(event) => onChange(event.target.value, 'description')}
                    />
                </div>
                <div className='buttons-controller-3'>
                    <button onClick={saveOrUpdateRequest} className='btn-main-2'>Створити</button>
                    <Link to='/state-payments'><button className='btn-trs-2'>Скасувати</button></Link>

                </div>
            </div>


        </div>
    </>
}