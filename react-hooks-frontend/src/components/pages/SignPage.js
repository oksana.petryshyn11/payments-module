import './SignPage.styles.css';
import logoHelp from '../logoHelp.svg';
import React, { useState } from 'react'
import SignService from '../../services/SignService';
import { useHistory } from "react-router-dom";
import { useDispatch } from 'react-redux';
import Select from 'react-select';
import { setAuth } from '../store/auth/reducer';

export const roles = [{
    label: 'user',
    value: 'user'
}, {
    label: 'specialist',
    value: 'SPECIALIST'
},
{
    label: 'accountant',
    value: 'ACCOUNTANT'
}, {
    label: 'admin',
    value: 'ADMIN'
}];

export const defaultValuesSignUp = {
    "name": "",
    "surname": "",
    "birthdate": "",
    "phoneNumber": "",
    "username": "",
    "email": "",
    "address": "",
    "telegramId": "",
    "passportNo": "",
    "iban": "",
    "password": "",
    "role": []
}

export const defaultValuesLogIn = {
    "username": "",
    "password": "",
}

export const SignPage = () => {
    const history = useHistory();
    const dispatch = useDispatch();

    const [activeLoginTab, SetActiveLoginTab] = useState(true)

    const [state, setState] = useState(defaultValuesSignUp);

    const [stateLogIn, setStateLogIn] = useState(defaultValuesLogIn);

    const onChange = (newValue, key) => setState((prev) => ({
        ...prev,
        [key]: newValue,
    }));

    const confirm = (e) => {
        e.preventDefault();

        if (activeLoginTab) {
            const request = {
                ...stateLogIn,
            };
            console.log('here')

            console.log(request);

            SignService.signIn(request).then((response) => {

                console.log(response.data);

                dispatch(setAuth(response.data));

                setStateLogIn(defaultValuesLogIn);

                setState(defaultValuesSignUp);
                history.push('/home');

            }).catch(error => {
                console.log(error)
            })
        } else {

            console.log('here-2')
            const request = {
                ...state,
                role: [state.role.label]
            };

            const request2 = {
                username: state.username,
                password: state.password
            };

            console.log(request)

            SignService.signUp(request).then((response) => {

                console.log(response.data);

            }).catch(error => {
                console.log(error)
            })

            SignService.signIn(request2).then((response) => {

                console.log(response.data);

                dispatch(setAuth(response.data));

                setStateLogIn(defaultValuesLogIn);

                setState(defaultValuesSignUp);
                history.push('/home');

            }).catch(error => {
                console.log(error)
            })
        }


    }

    const onSignUpTabClick = () => {
        SetActiveLoginTab(false);
    }

    const onLoginTabClick = () => {
        SetActiveLoginTab(true);
    }

    const onChangeLogIn = (newValue, key) => setStateLogIn((prev) => ({
        ...prev,
        [key]: newValue,
    }));

    console.log(state);

    return <>
        <div className='root-box'>
            <div>
                <div className='logo-content'>
                    <img className='logo-img' src={logoHelp} alt="Logo" />
                    <p className='logo-text-sign'>Є-Допомога</p>
                </div>
                <p className='text-main'>Не витрачайте час на черги. Ми допоможемо.</p>
            </div>

            <div>
                <div className='tab-buttons'>
                    <button onClick={onLoginTabClick} className={activeLoginTab ? 'active-btn' : ''}>Log In</button>
                    <button onClick={onSignUpTabClick} className={activeLoginTab ? '' : 'active-btn'}>Sign Up</button>
                </div>
                <hr />

                {activeLoginTab && (<><input
                    className='field-input'
                    placeholder='логін'
                    value={stateLogIn.username}
                    onChange={(event) => onChangeLogIn(event.target.value, 'username')}
                />

                    <input
                        className='field-input'
                        placeholder='пароль'
                        type="password"
                        value={stateLogIn.password}
                        onChange={(event) => onChangeLogIn(event.target.value, 'password')}
                    /></>)}

                {!activeLoginTab && (<div className='signin-cont'>
                    <div>
                        <p className='label-input'>Username:</p>
                        <input
                            className='field-input'
                            placeholder="username"
                            value={state.username}
                            type="text"
                            onChange={(event) => onChange(event.target.value, 'username')}
                        />
                        <p className='label-input'>Пароль:</p>
                        <input
                            className='field-input'
                            placeholder='пароль'
                            value={state.password}
                            type="password"
                            onChange={(event) => onChange(event.target.value, 'password')}
                        />
                        <p className='label-input'>Iм'я:</p>
                        <input
                            className='field-input'
                            placeholder="ім'я"
                            value={state.name}
                            type="text"
                            onChange={(event) => onChange(event.target.value, 'name')}
                        />
                        <p className='label-input'>Прізвище:</p>
                        <input
                            className='field-input'
                            placeholder="прізвище"
                            value={state.surname}
                            type="text"
                            onChange={(event) => onChange(event.target.value, 'surname')}
                        />
                    </div>
                    <div>
                        <p className='label-input'>Дата народження:</p>
                        <input
                            className='field-input'
                            placeholder="дата народження"
                            value={state.birthdate}
                            type="date"
                            onChange={(event) => onChange(event.target.value, 'birthdate')}
                        />
                        <p className='label-input'>Email:</p>
                        <input
                            className='field-input'
                            placeholder="email"
                            value={state.email}
                            type="email"
                            onChange={(event) => onChange(event.target.value, 'email')}
                        />
                        <p className='label-input'>Адреса:</p>
                        <input
                            className='field-input'
                            placeholder="адреса"
                            value={state.address}
                            onChange={(event) => onChange(event.target.value, 'address')}
                        />
                        <p className='label-input'>Телеграм Id:</p>
                        <input
                            className='field-input'
                            placeholder="телеграм id"
                            value={state.telegramId}
                            onChange={(event) => onChange(event.target.value, 'telegramId')}
                        />
                    </div>
                    <div>
                        <p className='label-input'>Номер телефону:</p>
                        <input
                            className='field-input'
                            placeholder="номер телефону"
                            value={state.phoneNumber}
                            onChange={(event) => onChange(event.target.value, 'phoneNumber')}
                        />
                        <p className='label-input'>Номер паспорту:</p>
                        <input
                            className='field-input'
                            placeholder="номер паспорту"
                            value={state.passportNo}
                            onChange={(event) => onChange(event.target.value, 'passportNo')}
                        />
                        <p className='label-input'>Номер рахунку:</p>
                        <input
                            className='field-input'
                            placeholder="номер рахунку"
                            value={state.iban}
                            onChange={(event) => onChange(event.target.value, 'iban')}
                        />
                        <p className='label-input'>Роль:</p>
                        <Select
                            value={state.role}
                            onChange={(value) => onChange(value, 'role')}
                            options={roles}
                            placeholder="role"
                            className='selector-signIn'
                        />
                    </div>
                </div>)}

                <button onClick={confirm} className='enter-btn'>{activeLoginTab ? 'Вхід' : 'Реєстрація'}</button>
            </div>
        </div>
    </>
}