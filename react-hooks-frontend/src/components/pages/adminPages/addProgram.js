import './addProgram.styles.css';
import React, { useState } from 'react'
import { useSelector } from 'react-redux';
import ArrowLeft from '.././ArrowLeft.svg';
import { Link } from 'react-router-dom';
import { useHistory } from "react-router-dom";
import { selectToken } from '../../store/auth';
import PaymentProgram from '../../../services/PaymentProgram';
import { ProgramsInfo } from '../../programsInfo';

export const defaultValues = {
    "name": "",
    "description": "",
    "isActive": false,
    "amount": 0
}

export const AddProgram = () => {
    const history = useHistory();
    const token = useSelector(selectToken);
    const [state, setState] = useState(defaultValues);

    const onChange = (newValue, key) => setState((prev) => ({
        ...prev,
        [key]: newValue,
    }));

    const createProgram = (e) => {
        e.preventDefault();

        PaymentProgram.addPaymentProgram(state, token).then((response) => {

            history.push('/state-payments');

        }).catch(error => {
            console.log(error)
        })
    };

    const onCancel = () => {
        history.push('/state-payments');
    }

    return <>
        <div className='header-rqst'>
            <div>
                <p className='text-request'><span>Усі заяви / </span>Створити програму</p>
                <div className='back'>
                    <Link to='/state-payments'>
                        <img
                            className='arrow-left'
                            src={ArrowLeft} alt="Arrow" /></Link>

                    <h3>Нова програма</h3>
                </div>
            </div>
            <ProgramsInfo />
        </div>


        <div className='create-request'>
            <div className='content-request-admin'>
                <div>
                    <p className='label-input'>Назва:</p>
                    <input
                        className='field-input'
                        placeholder="назва"
                        value={state.name}
                        type="text"
                        onChange={(event) => onChange(event.target.value, 'name')}
                    />
                    <p className='label-input'>Опис:</p>
                    <textarea
                        className='field-area field-input'
                        placeholder='опис'
                        value={state.description}
                        type="password"
                        onChange={(event) => onChange(event.target.value, 'description')}
                    />
                    <p className='label-input'>Сума виплати</p>
                    <input
                        className='field-input'
                        placeholder="сума виплати"
                        value={state.amount}
                        type="number"
                        onChange={(event) => onChange(event.target.value, 'amount')}
                    />
                    <p className='label-input'>Status:</p>
                    <div className='check-box'>
                        <label className='label-check'>isActive</label>
                        <input
                            className='field-check'
                            value={state.isActive}
                            type="checkbox"
                            label="isActive"
                            onChange={() => onChange(!state.isActive, 'isActive')}
                        />
                    </div>
                </div>
            </div>

            <div className='buttons-controller-4'>
                <button className='btn-main-4' onClick={createProgram}>Додати</button>
                <button onClick={onCancel} className='btn-trs-4'>Скасувати</button>
            </div>
        </div>
    </>
}