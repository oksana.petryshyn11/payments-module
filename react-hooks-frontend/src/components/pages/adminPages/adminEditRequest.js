import './adminEditRequest.styles.css';
import React, { useEffect, useState } from 'react'
import RequestService from '../../../services/RequestService';
import { useSelector } from 'react-redux';
import ArrowLeft from '.././ArrowLeft.svg';
import { Link, useParams } from 'react-router-dom';
import { useHistory } from "react-router-dom";
import { selectRoles, selectToken } from '../../store/auth';
import { ProgramsInfo } from '../../programsInfo';

export const defaultValues = {
    "id": 0,
    "user": {
        "id": 0,
        "name": "",
        "surname": "",
        "birthdate": "",
        "phoneNumber": "",
        "email": "",
        "address": "",
        "telegramId": 0,
        "roles": [
            {
                "id": 0,
                "name": "ROLE_USER"
            }
        ],
        "username": "nastia1",
        "password": "",
        "passportNo": "",
        "iban": ""
    },
    "paymentProgram": {
        "id": 0,
        "name": "",
        "description": "",
        "amount": 0,
        "isActive": true
    },
    "description": "",
    "status": "CANCELLED",
    "priority": "DEFAULT",
    "lastUpdate": ""
}

export const AdminEditRequest = () => {
    const history = useHistory();
    const token = useSelector(selectToken);
    const { requestId = '' } = useParams();
    const role = useSelector(selectRoles)[0];
    const [state, setState] = useState(defaultValues);

    const [request, setRequest] = useState({});

    useEffect(() => {
        if (requestId) {
            getRequest();
        }
    }, [requestId])

    const getRequest = () => {
        RequestService.getRequestById(requestId, token).then((response) => {
            setRequest(response.data);

            setState({
                ...response.data,
            })
            console.log(response.data);
        }).catch(error => {
            console.log(error);
        })
    }

    const onChange = (newValue, key) => setState((prev) => ({
        ...prev,
        [key]: newValue,
    }));

    const changeStatus = (e) => {
        e.preventDefault();

        if (role === 'ROLE_ACCOUNTANT') {
            RequestService.updateRequestWithStatus(requestId, 'DONE', token).then((response) => {

                history.push('/state-payments');

            }).catch(error => {
                console.log(error)
            })
            return;
        }

        if (state.status === 'CREATED') {

            RequestService.updateRequestWithStatus(requestId, 'IN_REVIEW', token).then((response) => {

                history.push('/state-payments');

            }).catch(error => {
                console.log(error)
            })
        } else {
            RequestService.updateRequestWithStatus(requestId, 'APPROVED', token).then((response) => {

                history.push('/state-payments');

            }).catch(error => {
                console.log(error)
            })
        }
    };

    const changeRejStatus = (e) => {
        e.preventDefault();

        if (role === 'ROLE_SPECIALIST') {
            RequestService.updateRequestWithStatus(requestId, 'CANCELLED', token).then((response) => {

                history.push('/state-payments');

            }).catch(error => {
                console.log(error)
            })
            return;
        }
    }

    const onCancel = () => {
        history.push('/state-payments');
    }

    return <>
        <div className='header-rqst'>
            <p className='text-request'><span>Усі заяви / </span>Заява</p>
            <div className='back'>
                <Link to='/state-payments'>
                    <img
                        className='arrow-left'
                        src={ArrowLeft} alt="Arrow" /></Link>

                <h3>Заява № {request.id}</h3>
            </div>
            <ProgramsInfo />
        </div>


        <div className='create-request'>
            <div className='content-request-admin'>
                <div>
                    <p className='label-input'>Username:</p>
                    <input
                        className='field-input'
                        placeholder="username"
                        disabled
                        value={state.user.username}
                        type="text"
                        onChange={(event) => onChange(event.target.value, 'username')}
                    />
                    <p className='label-input'>Iм'я:</p>
                    <input
                        className='field-input'
                        placeholder="ім'я"
                        disabled
                        value={state.user.name}
                        type="text"
                        onChange={(event) => onChange(event.target.value, 'name')}
                    />
                    <p className='label-input'>Прізвище:</p>
                    <input
                        className='field-input'
                        placeholder="прізвище"
                        disabled
                        value={state.user.surname}
                        type="text"
                        onChange={(event) => onChange(event.target.value, 'surname')}
                    />
                    <p className='label-input'>Номер телефону:</p>
                    <input
                        className='field-input'
                        placeholder="номер телефону"
                        value={state.user.phoneNumber}
                        disabled
                        onChange={(event) => onChange(event.target.value, 'phoneNumber')}
                    />
                </div>
                <div>
                    <p className='label-input'>Дата народження:</p>
                    <input
                        className='field-input'
                        placeholder="дата народження"
                        value={state.user.birthdate}
                        disabled
                        type="date"
                        onChange={(event) => onChange(event.target.value, 'birthdate')}
                    />
                    <p className='label-input'>Email:</p>
                    <input
                        className='field-input'
                        placeholder="email"
                        disabled
                        value={state.user.email}
                        type="email"
                        onChange={(event) => onChange(event.target.value, 'email')}
                    />
                    <p className='label-input'>Адреса:</p>
                    <input
                        className='field-input'
                        placeholder="адреса"
                        value={state.user.address}
                        disabled
                        onChange={(event) => onChange(event.target.value, 'address')}
                    />
                    <p className='label-input'>Номер паспорту:</p>
                    <input
                        className='field-input'
                        placeholder="номер паспорту"
                        value={state.user.passportNo}
                        disabled
                        onChange={(event) => onChange(event.target.value, 'passportNo')}
                    />
                </div>
                <div>
                    <p className='label-input'>Номер рахунку:</p>
                    <input
                        className='field-input'
                        placeholder="номер рахунку"
                        value={state.user.iban}
                        disabled
                        onChange={(event) => onChange(event.target.value, 'iban')}
                    />
                    <p className='label-input'>Категорія:</p>
                    <input
                        className='field-input'
                        placeholder='катерогія'
                        value={state.paymentProgram.name}
                        disabled
                        onChange={(event) => onChange(event.target.value, 'password')}
                    />
                    <p className='label-input'>Опис:</p>
                    <textarea
                        className='field-area field-input'
                        placeholder='опис'
                        value={state.description}
                        type="password"
                        disabled
                        onChange={(event) => onChange(event.target.value, 'password')}
                    />
                </div>
            </div>

            <div className='buttons-controller-4'>
                {role === 'ROLE_ACCOUNTANT' && <>{state.status === 'APPROVED' && <button className='btn-main-4' onClick={changeStatus}>Сплатити</button>}</>}
                {role === 'ROLE_SPECIALIST' && <>{state.status === 'CREATED' && <button className='btn-main-4' onClick={changeStatus}>На перевірку</button>}
                    {state.status === 'IN_REVIEW' && <button className='btn-main-4' onClick={changeStatus}>Схвалити</button>}</>}

                {(state.status === 'IN_REVIEW' && role === 'ROLE_SPECIALIST') && <button className='btn-main-4' onClick={changeRejStatus}>Відмовити</button>}
                <button onClick={onCancel} className='btn-trs-4'>Скасувати</button>
            </div>
        </div>
    </>
}