import './adminRequest.styles.css';
import React, { useEffect, useState, useCallback } from 'react'
import RequestService from '../../../services/RequestService';
import { useHistory, Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { selectToken, selectRoles, } from '../../store/auth';

export const AdminRequest = () => {
    const token = useSelector(selectToken);
    const role = useSelector(selectRoles)[0];
    const [requests, setRequests] = useState([]);
    const history = useHistory();

    useEffect(() => {

        getAllRequests();
    }, [])

    const getAllRequests = () => {
        RequestService.getAllRequests(token).then((response) => {
            setRequests(response.data)
            console.log(response.data);
        }).catch(error => {
            console.log(error);
        })
    }

    const addProgram = () => {
        history.push('/state-payments/add-program');
    }

    const [upSort, setUpsort] = useState(false);

    const sortRequests = useCallback(() => {
        setRequests(requests.sort((a, b) => upSort ? a.id - b.id : b.id - a.id));
        setUpsort((prev) => !prev);
    }, [requests, upSort]);

    return <>
        <div className='page-header'>
            <h3>Усі заяви</h3>
            {role === 'ROLE_ADMIN' && <button className='btn-main-5' onClick={addProgram}>Додати програму</button>}
        </div>

        <table className="requests-table">
            <thead>
                <th className='sortable' onClick={sortRequests}>Номер</th>
                <th>Категорія</th>
                <th>Опис</th>
                <th>Прізвище та Ім'я</th>
                <th className='status'>Статус</th>
                <th>Дата оновлення</th>
            </thead>
            <tbody>
                {
                    requests.map(
                        request =>
                            <tr key={request.id}>
                                <Link to={`/state-payments/edit-request/${request.id}`}><td>{request.id}</td></Link>
                                <td>{request.paymentProgram.name}</td>
                                <td>{request.description}</td>
                                <td>{request.user.surname} {request.user.name}</td>
                                <td className='status'>
                                    <button className={
                                        request.status === 'IN_REVIEW'
                                            ? 'active'
                                            : request.status === 'CANCELLED'
                                                ? 'reject'
                                                : 'confirm'}> {request.status}
                                    </button>
                                </td>
                                <td>{request.lastUpdate}</td>
                            </tr>
                    )
                }
            </tbody>
        </table>
    </>
}