import './createRequestPage.styles.css';
import React, { useEffect, useState, useMemo, useCallback } from 'react'
import RequestService from '../../services/RequestService';
import { RequestPageHeader } from '../RequestPageHeader';
import DeleteIcon from './DeleteIcon.svg';
import { useSelector } from 'react-redux';
import { selectToken } from '../store/auth';

export const CreateRequestPage = () => {
    const token = useSelector(selectToken);
    const [requests, setRequests] = useState([])

    useEffect(() => {

        getAllRequests();
    }, [])

    const getAllRequests = () => {
        RequestService.getAllRequests(token).then((response) => {
            setRequests(response.data)
            console.log(response.data);
        }).catch(error => {
            console.log(error);
        })
    }

    const deleteRequest = (requestId) => {
        RequestService.deleteRequest(requestId, token).then((response) => {
            getAllRequests();

        }).catch(error => {
            console.log(error);
        })
    }

    const formattedRequests = useMemo(() =>
        requests.filter((req) => req.status !== 'CANCELLED'),
        [requests]);
        
        const [upSort,setUpsort] = useState(false);

        const sortRequests = useCallback(() => {
            setRequests(formattedRequests.sort((a,b)=>upSort ? a.id-b.id : b.id-a.id));
            setUpsort((prev) => !prev);
        },[formattedRequests, upSort]); 

    return <>
        <RequestPageHeader
            onCreateClick={() => console.log('create')}
            onProgramsClick={() => console.log('programs')}
        />

        <table className="requests-table">
            <thead>
                <th className='sortable' onClick={sortRequests}>Номер</th>
                <th>Категорія</th>
                <th>Опис</th>
                <th className='status'>Статус</th>
                <th>Дата оновлення</th>
                <th>Дії</th>
            </thead>
            <tbody>
                {
                    formattedRequests.map(
                        request =>
                            <tr key={request.id}>
                                <td>{request.id}</td>
                                <td>{request.paymentProgram.name}</td>
                                <td>{request.description}</td>
                                <td className='status'>
                                    <button className={
                                        request.status === 'IN_REVIEW'
                                            ? 'active'
                                            : request.status === 'CANCELLED'
                                                ? 'reject'
                                                : 'confirm'}> {request.status}
                                    </button>
                                </td>
                                <td>{request.lastUpdate}</td>
                                <td>
                                    <button
                                        className="delete-btn"
                                        onClick={() => deleteRequest(request.id)}>
                                        <img className='img-icon' src={DeleteIcon} alt="Delete" />
                                    </button>
                                </td>
                            </tr>
                    )
                }
            </tbody>
        </table>
    </>
}