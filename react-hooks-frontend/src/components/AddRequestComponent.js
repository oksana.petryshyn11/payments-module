import React, {useEffect, useState} from 'react'
import {Link, useHistory, useParams} from 'react-router-dom';
import RequestService from '../services/RequestService'

const AddRequestComponent = () => {

    const [reason, setReason] = useState('')
    const history = useHistory();
    const {id} = useParams();

    const saveOrUpdateRequest = (e) => {
        e.preventDefault();

        const request = {reason}

        if (id) {
            RequestService.updateRequest(id, request).then((response) => {
                history.push('/requests')
            }).catch(error => {
                console.log(error)
            })

        } else {
            RequestService.createRequest(request).then((response) => {

                console.log(response.data)

                history.push('/requests');

            }).catch(error => {
                console.log(error)
            })
        }

    }

    useEffect(() => {

        RequestService.getRequestById(id).then((response) => {
            // TODO: User adding
            setReason(response.data.reason)
        }).catch(error => {
            console.log(error)
        })
    }, [])

    const title = () => {

        if (id) {
            return <h2 className="text-center">Update Request</h2>
        } else {
            return <h2 className="text-center">Add Request</h2>
        }
    }

    return (
        <div>
            <br/><br/>
            <div className="container">
                <div className="row">
                    <div className="card col-md-6 offset-md-3 offset-md-3">
                        {
                            title()
                        }
                        <div className="card-body">
                            <form>
                                <div className="form-group mb-2">
                                    <label className="form-label"> Reason :</label>
                                    <input
                                        type="text"
                                        placeholder="Enter reason"
                                        name="reason"
                                        className="form-control"
                                        value={reason}
                                        onChange={(e) => setReason(e.target.value)}
                                    >
                                    </input>
                                </div>

                                {/*<div className = "form-group mb-2">*/}
                                {/*    <label className = "form-label"> Last Name :</label>*/}
                                {/*    <input*/}
                                {/*        type = "text"*/}
                                {/*        placeholder = "Enter last name"*/}
                                {/*        name = "lastName"*/}
                                {/*        className = "form-control"*/}
                                {/*        value = {lastName}*/}
                                {/*        onChange = {(e) => setLastName(e.target.value)}*/}
                                {/*    >*/}
                                {/*    </input>*/}
                                {/*</div>*/}

                                {/*<div className = "form-group mb-2">*/}
                                {/*    <label className = "form-label"> Email Id :</label>*/}
                                {/*    <input*/}
                                {/*        type = "email"*/}
                                {/*        placeholder = "Enter email Id"*/}
                                {/*        name = "emailId"*/}
                                {/*        className = "form-control"*/}
                                {/*        value = {reason}*/}
                                {/*        onChange = {(e) => setReason(e.target.value)}*/}
                                {/*    >*/}
                                {/*    </input>*/}
                                {/*</div>*/}

                                <button className="btn btn-success" onClick={(e) => saveOrUpdateRequest(e)}>Submit
                                </button>
                                <Link to="/requests" className="btn btn-danger"> Cancel </Link>
                            </form>

                        </div>
                    </div>
                </div>

            </div>

        </div>
    )
}

export default AddRequestComponent
