import mainPageIcon from './mainPageIcon.svg';

export const cardsUtils = [{
    path: '/home',
    title: 'Головна',
    icon: mainPageIcon
},
{
    path: '/dwelling',
    title: 'Житло',
    icon: ''
},
{
    path: '/medicine-&-education',
    title: 'Медицина та освіта',
    icon: ''
},
{
    path: '/assistance-to-organizations',
    title: 'Розподілення фінансів',
    icon: ''
},
{
    path: '/repurposing',
    title: 'Перепрофілювання',
    icon: ''
},
{
    path: '/work',
    title: 'Робота',
    icon: ''
},
{
    path: '/psychological-help',
    title: 'Психологічна допомога',
    icon: ''
},
{
    path: '/state-payments',
    title: 'Державні виплати',
    icon: ''
},
{
    path: '/material-assistance',
    title: 'Речова допомога',
    icon: ''
}];