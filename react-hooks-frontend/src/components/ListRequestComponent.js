import React, {useEffect, useState} from 'react'
import {Link} from 'react-router-dom'
import RequestService from '../services/RequestService'

const ListRequestComponent = () => {

    const [requests, setRequests] = useState([])

    useEffect(() => {

        getAllRequests();
    }, [])

    const getAllRequests = () => {
        RequestService.getAllRequests().then((response) => {
            setRequests(response.data)
            console.log(response.data);
        }).catch(error => {
            console.log(error);
        })
    }

    const deleteRequest = (requestId) => {
        RequestService.deleteRequest(requestId).then((response) => {
            getAllRequests();

        }).catch(error => {
            console.log(error);
        })

    }

    return (
        <div className="container">
            <h2 className="text-center"> List Requests </h2>
            <Link to="/add-request" className="btn btn-primary mb-2"> Add Request </Link>
            <table className="table table-bordered table-striped">
                <thead>
                <th> Request Id</th>
                <th> Request Reason</th>
                <th> Request Status</th>
                <th> Request User</th>
                <th> Request Payed timestamp</th>
                <th> Actions</th>
                </thead>
                <tbody>
                {
                    requests.map(
                        request =>
                            <tr key={request.id}>
                                <td> {request.id} </td>
                                <td> {request.reason} </td>
                                <td> {request.status} </td>
                                <td> {request.user.name}</td>
                                <td> {request.payedDatetime}</td>
                                <td>
                                    <Link className="btn btn-info" to={`/edit-request/${request.id}`}>Update</Link>
                                    <button className="btn btn-danger" onClick={() => deleteRequest(request.id)}
                                            style={{marginLeft: "10px"}}> Delete
                                    </button>
                                </td>
                            </tr>
                    )
                }
                </tbody>
            </table>
        </div>
    )
}

export default ListRequestComponent
