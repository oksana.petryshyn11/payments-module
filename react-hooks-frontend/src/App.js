import './App.css';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './components/store/store';
import { Content } from './content';
import { createBrowserHistory } from 'history';

function App() {
    const history = createBrowserHistory({ window })

    return (
        <Provider store={store}>
            <Router history={history}>
                <Content />
            </Router>
        </Provider>
    );
}

export default App;
