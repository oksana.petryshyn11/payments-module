package org.help.payments.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.help.payments.model.PaymentProgram;
import org.help.payments.service.PaymentProgramService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@WebMvcTest(value = PaymentProgramController.class)
class PaymentProgramControllerTest {

    private final ObjectWriter writer = new ObjectMapper().configure(SerializationFeature.WRAP_ROOT_VALUE, false).writer().withDefaultPrettyPrinter();
    private final PaymentProgram paymentProgram = PaymentProgram.builder()
            .name("test program")
            .isActive(true)
            .description("test program description")
            .amount(2300)
            .build();
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private PaymentProgramService paymentProgramService;

    @WithMockUser
    @Test
    public void getPaymentPrograms() throws Exception {

        // Given
        String expectedResponse = "[{\"id\":0,\"name\":\"test program\",\"description\":\"test program description\",\"amount\":2300,\"isActive\":true}]";
        Mockito.when(
                paymentProgramService.getAll()).thenReturn(List.of(paymentProgram));

        // When
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                        "/api/v1/payment_program")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        // Then
        JSONAssert.assertEquals(expectedResponse, result.getResponse().getContentAsString(), false);
    }

    @WithMockUser
    @Test
    void getPaymentProgramById() throws Exception {
        // Given
        Mockito.when(
                paymentProgramService.getPaymentProgramById(1)).thenReturn(paymentProgram);

        // When
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                        "/api/v1/payment_program/1")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        // Then
        String expectedResponse = "{\"id\":0,\"name\":\"test program\",\"description\":\"test program description\",\"amount\":2300,\"isActive\":true}";
        JSONAssert.assertEquals(expectedResponse, result.getResponse().getContentAsString(), false);
    }

    @Test
    @WithMockUser(username = "admin", roles = {"USER", "ADMIN"})
    void createPaymentProgram() throws Exception {
        // Given
        Mockito.when(
                paymentProgramService.add(paymentProgram)).thenReturn(paymentProgram);

        // When
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
                        "/api/v1/payment_program")
                .accept(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(paymentProgram))
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON);

        // Then
        mockMvc.perform(requestBuilder).andExpect(status().isOk());

    }

    @WithMockUser(username = "admin", roles = {"USER", "ADMIN"})
    @Test
    void updatePaymentProgram() throws Exception {
        // Given
        Mockito.when(
                paymentProgramService.update(1, paymentProgram)).thenReturn(paymentProgram);

        // When
        RequestBuilder requestBuilder = MockMvcRequestBuilders.put(
                        "/api/v1/payment_program/1")
                .content(writer.writeValueAsString(paymentProgram))
                .accept(MediaType.APPLICATION_JSON)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON);

        // Then
        mockMvc.perform(requestBuilder).andExpect(status().isOk());
    }
}