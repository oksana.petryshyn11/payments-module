package org.help.payments.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.help.payments.model.jwt.LoginRequest;
import org.help.payments.model.jwt.SignupRequest;
import org.help.payments.security.JwtUtils;
import org.help.payments.security.UserDetailsImpl;
import org.help.payments.service.UserRoleService;
import org.help.payments.service.UserService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.sql.Date;
import java.util.Set;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@WebMvcTest(value = AuthController.class)
public class AuthControllerTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private AuthenticationManager authenticationManager;
    @MockBean
    private UserService userService;
    @MockBean
    private UserRoleService userRoleService;
    @MockBean
    private PasswordEncoder encoder;
    @MockBean
    private JwtUtils jwtUtils;
    @Mock
    private Authentication authentication;
    @Mock
    private UserDetailsImpl userDetails;

    @Before
    public void initMocks() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @WithMockUser
    public void authenticateUser() throws Exception {
        // Given
        String jwt = "jwt";
        Mockito.when(
                authenticationManager.authenticate(new UsernamePasswordAuthenticationToken("oksana", "pass"))).thenReturn(authentication);
        Mockito.when(jwtUtils.generateJwtToken(authentication)).thenReturn(jwt);
        Mockito.when(authentication.getPrincipal()).thenReturn(userDetails);
        Mockito.when(userDetails.getUsername()).thenReturn("oksana");
        Mockito.when(userDetails.getEmail()).thenReturn("oksana@gmail.com");

        String expectedResponse = "{\"roles\":[],\"token\":\"jwt\",\"id\":0,\"username\":\"oksana\",\"email\":\"oksana@gmail.com\"}";

        // When
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
                        "/api/v1/auth/signin")
                .accept(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(new LoginRequest("oksana", "pass")))
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        // Then
        JSONAssert.assertEquals(expectedResponse, result.getResponse().getContentAsString(), false);
    }

    @Test
    @WithMockUser
    public void registerUser() throws Exception {
        // Given
        SignupRequest signupRequest = new SignupRequest("opetryshyn", "oksanapetryshyn@gmail.com", Set.of("specialist"),
                "Oksana", "Petryshyn", Date.valueOf("2002-10-11"), "0505193844", "Lviv",
                4123912313L, "FU929329", "UA219303291034124", "password");
        String jwt = "jwt";
        Mockito.when(
                userService.existsByEmail("oksanapetryshyn@gmail.com")).thenReturn(false);
        Mockito.when(userService.existsByUsername("opetryshyn")).thenReturn(false);

        String expectedResponse = "{\"message\":\"User registered successfully!\"}";

        // When
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
                        "/api/v1/auth/signup")
                .accept(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(signupRequest))
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        // Then
        JSONAssert.assertEquals(expectedResponse, result.getResponse().getContentAsString(), false);
    }
}