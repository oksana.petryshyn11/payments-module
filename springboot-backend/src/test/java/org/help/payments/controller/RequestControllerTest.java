package org.help.payments.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.help.payments.model.*;
import org.help.payments.service.RequestService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@WebMvcTest(value = RequestController.class)
class RequestControllerTest {

    private final ObjectWriter writer = new ObjectMapper().configure(SerializationFeature.WRAP_ROOT_VALUE, false).writer().withDefaultPrettyPrinter();
    private final User user = User.builder()
            .name("Oksana")
            .passportNo("FU030301")
            .surname("Petryshyn")
            .phoneNumber("+380505173824")
            .build();
    private final PaymentProgram paymentProgram = PaymentProgram.builder()
            .amount(1200)
            .isActive(true)
            .name("test program")
            .build();
    private final Request request = Request.builder()
            .description("need food")
            .user(user)
            .paymentProgram(paymentProgram)
            .status(RequestStatusEnum.CREATED)
            .priority(RequestPriorityEnum.DEFAULT)
            .build();
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private RequestService requestService;

    @Test
    @WithMockUser
    void getRequests() throws Exception {
        // Given
        Mockito.when(
                requestService.getRequests("token")).thenReturn(List.of(request));

        String expectedResponse = "[{\"id\":0,\"user\":{\"id\":0,\"name\":\"Oksana\",\"surname\":\"Petryshyn\"," +
                "\"birthdate\":null,\"phoneNumber\":\"+380505173824\",\"email\":null,\"address\":null,\"telegramId\":0," +
                "\"roles\":null,\"username\":null,\"password\":null,\"passportNo\":\"FU030301\",\"iban\":null}," +
                "\"paymentProgram\":{\"id\":0,\"name\":\"test program\",\"description\":null,\"amount\":1200," +
                "\"isActive\":true},\"description\":\"need food\",\"status\":\"CREATED\",\"priority\":\"DEFAULT\"," +
                "\"lastUpdate\":null}]";
        // When
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                        "/api/v1/requests")
                .header("Authorization", "token")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        // Then
        JSONAssert.assertEquals(expectedResponse, result.getResponse().getContentAsString(), false);
    }

    @Test
    @WithMockUser
    void createRequest() throws Exception {
        // Given
        Mockito.when(
                requestService.createRequest("token", "Program", "Description")).thenReturn(request);

        String expectedResponse = "{\"id\":0,\"user\":{\"id\":0,\"name\":\"Oksana\",\"surname\":\"Petryshyn\"," +
                "\"birthdate\":null,\"phoneNumber\":\"+380505173824\",\"email\":null,\"address\":null,\"telegramId\":0," +
                "\"roles\":null,\"username\":null,\"password\":null,\"passportNo\":\"FU030301\",\"iban\":null}," +
                "\"paymentProgram\":{\"id\":0,\"name\":\"test program\",\"description\":null,\"amount\":1200," +
                "\"isActive\":true},\"description\":\"need food\",\"status\":\"CREATED\",\"priority\":\"DEFAULT\",\"lastUpdate\":null}";

        // When
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
                        "/api/v1/requests")
                .header("Authorization", "token")
                .accept(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(new RequestDetails("Program", "Description")))
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        // Then
        JSONAssert.assertEquals(expectedResponse, result.getResponse().getContentAsString(), false);
    }

    @Test
    @WithMockUser
    void getRequestById() throws Exception {
        // Given
        Mockito.when(
                requestService.getRequestById(1)).thenReturn(request);

        String expectedResponse = "{\"id\":0,\"user\":{\"id\":0,\"name\":\"Oksana\",\"surname\":\"Petryshyn\",\"birthdate\":null," +
                "\"phoneNumber\":\"+380505173824\",\"email\":null,\"address\":null,\"telegramId\":0,\"roles\":null," +
                "\"username\":null,\"password\":null,\"passportNo\":\"FU030301\",\"iban\":null},\"paymentProgram\":" +
                "{\"id\":0,\"name\":\"test program\",\"description\":null,\"amount\":1200,\"isActive\":true}," +
                "\"description\":\"need food\",\"status\":\"CREATED\",\"priority\":\"DEFAULT\",\"lastUpdate\":null}";
        // When
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                        "/api/v1/requests/1")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        // Then
        JSONAssert.assertEquals(expectedResponse, result.getResponse().getContentAsString(), false);
    }

    @Test
    @WithMockUser(username = "spec", roles = {"SPECIALIST"})
    void updateRequestStatus() throws Exception {
        // Given
        Mockito.when(
                requestService.updateRequestWithStatus(1, "APPROVED")).thenReturn(request);

        String expectedResponse = "{\"id\":0,\"user\":{\"id\":0,\"name\":\"Oksana\",\"surname\":\"Petryshyn\"," +
                "\"birthdate\":null,\"phoneNumber\":\"+380505173824\",\"email\":null,\"address\":null,\"telegramId\":0," +
                "\"roles\":null,\"username\":null,\"password\":null,\"passportNo\":\"FU030301\",\"iban\":null}," +
                "\"paymentProgram\":{\"id\":0,\"name\":\"test program\",\"description\":null,\"amount\":1200," +
                "\"isActive\":true},\"description\":\"need food\",\"status\":\"CREATED\",\"priority\":\"DEFAULT\",\"lastUpdate\":null}";

        // When
        RequestBuilder requestBuilder = MockMvcRequestBuilders.put(
                        "/api/v1/requests/change_status/1")
                .accept(MediaType.APPLICATION_JSON)
                .param("status", "APPROVED")
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        // Then
        JSONAssert.assertEquals(expectedResponse, result.getResponse().getContentAsString(), false);
    }

    @Test
    @WithMockUser(username = "spec", roles = {"SPECIALIST"})
    void updateRequestPriority() throws Exception {
        // Given
        Mockito.when(
                requestService.updateRequestWithPriority(1, "LOW")).thenReturn(request);

        String expectedResponse = "{\"id\":0,\"user\":{\"id\":0,\"name\":\"Oksana\",\"surname\":\"Petryshyn\"," +
                "\"birthdate\":null,\"phoneNumber\":\"+380505173824\",\"email\":null,\"address\":null,\"telegramId\":0," +
                "\"roles\":null,\"username\":null,\"password\":null,\"passportNo\":\"FU030301\",\"iban\":null}," +
                "\"paymentProgram\":{\"id\":0,\"name\":\"test program\",\"description\":null,\"amount\":1200," +
                "\"isActive\":true},\"description\":\"need food\",\"status\":\"CREATED\",\"priority\":\"DEFAULT\",\"lastUpdate\":null}";

        // When
        RequestBuilder requestBuilder = MockMvcRequestBuilders.put(
                        "/api/v1/requests/change_priority/1")
                .accept(MediaType.APPLICATION_JSON)
                .param("priority", "LOW")
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        // Then
        JSONAssert.assertEquals(expectedResponse, result.getResponse().getContentAsString(), false);
    }

    @Test
    @WithMockUser
    void deleteRequest() throws Exception {
        // Given
        Mockito.when(
                requestService.updateRequestWithStatus(1, "CANCELLED")).thenReturn(request);

        String expectedResponse = "{\"id\":0,\"user\":{\"id\":0,\"name\":\"Oksana\",\"surname\":\"Petryshyn\"," +
                "\"birthdate\":null,\"phoneNumber\":\"+380505173824\",\"email\":null,\"address\":null,\"telegramId\":0," +
                "\"roles\":null,\"username\":null,\"password\":null,\"passportNo\":\"FU030301\",\"iban\":null}," +
                "\"paymentProgram\":{\"id\":0,\"name\":\"test program\",\"description\":null,\"amount\":1200," +
                "\"isActive\":true},\"description\":\"need food\",\"status\":\"CREATED\",\"priority\":\"DEFAULT\",\"lastUpdate\":null}";

        // When
        RequestBuilder requestBuilder = MockMvcRequestBuilders.put(
                        "/api/v1/requests/delete/1")
                .accept(MediaType.APPLICATION_JSON)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        // Then
        JSONAssert.assertEquals(expectedResponse, result.getResponse().getContentAsString(), false);
    }
}