package org.help.payments.service;

import org.help.payments.exception.ResourceNotFoundException;
import org.help.payments.model.PaymentProgram;
import org.help.payments.repository.PaymentProgramRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentProgramService {

    @Autowired
    private PaymentProgramRepository paymentProgramRepository;

    public PaymentProgram getPaymentProgramById(long id) {
        return paymentProgramRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Payment program does not exist with id:" + id));
    }

    public PaymentProgram getPaymentProgramByName(String name) {
        return paymentProgramRepository.findByName(name)
                .orElseThrow(() -> new ResourceNotFoundException("Payment program does not exist with name:" + name));
    }

    public List<PaymentProgram> getAll() {
        return paymentProgramRepository.findAll();
    }

    public PaymentProgram add(PaymentProgram paymentProgram) {
        return paymentProgramRepository.save(paymentProgram);
    }

    public PaymentProgram update(long id, PaymentProgram paymentProgram) {
        PaymentProgram existingProgram = getPaymentProgramById(id);
        existingProgram.setAmount(paymentProgram.getAmount());
        existingProgram.setDescription(paymentProgram.getDescription());
        existingProgram.setIsActive(paymentProgram.getIsActive());
        existingProgram.setName(paymentProgram.getName());
        return paymentProgramRepository.save(existingProgram);
    }
}
