package org.help.payments.service;

import org.help.payments.exception.ResourceNotFoundException;
import org.help.payments.model.*;
import org.help.payments.repository.RequestRepository;
import org.help.payments.security.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class RequestService {

    @Autowired
    private UserService userService;
    @Autowired
    private PaymentProgramService paymentProgramService;
    @Autowired
    private RequestRepository requestRepository;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private JwtUtils jwtUtils;

    private List<Request> getAllRequestsFilteredByStatuses(List<RequestStatusEnum> statuses) {
        return requestRepository.findAll()
                .stream()
                .filter(request -> statuses.contains(request.getStatus()))
                .toList();
    }

    public List<Request> getRequests(String token) {
        String username = jwtUtils.getUsernameFromJwtToken(jwtUtils.retrieveToken(token));

        User user = userService.getUserByUsername(username);

        UserRoleEnum role = user.getRoles().stream().toList().get(0).getName();

        return switch (role) {
            case ROLE_ADMIN -> requestRepository.findAll();
            case ROLE_ACCOUNTANT -> getAllRequestsFilteredByStatuses(List.of(RequestStatusEnum.APPROVED));
            case ROLE_SPECIALIST ->
                    getAllRequestsFilteredByStatuses(List.of(RequestStatusEnum.CREATED, RequestStatusEnum.IN_REVIEW));
            case ROLE_USER -> requestRepository.findAllByUserId(user.getId());
        };
    }

    public Request getRequestById(long id) {
        return requestRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Request does not exist with id:" + id));
    }

    public Request updateRequestWithStatus(long id, String status) {
        Request request = getRequestById(id);
        request.setStatus(RequestStatusEnum.valueOf(status));
        request.setLastUpdate(LocalDateTime.now());

        Notification notification = Notification.builder()
                .user(request.getUser())
                .request(request)
                .message("Статус заяви був змінений на " + request.getStatus())
                .creationTime(LocalDateTime.now())
                .build();
        notificationService.send(notification);
        notificationService.save(notification);

        return requestRepository.save(request);
    }

    public Request updateRequestWithPriority(long id, String priority) {
        Request request = getRequestById(id);
        request.setPriority(RequestPriorityEnum.valueOf(priority));
        request.setLastUpdate(LocalDateTime.now());

        Notification notification = Notification.builder()
                .user(request.getUser())
                .request(request)
                .message("Пріоритет заяви був змінений на " + request.getPriority())
                .creationTime(LocalDateTime.now())
                .build();
        notificationService.send(notification);
        notificationService.save(notification);

        return requestRepository.save(request);
    }

    public Request createRequest(String token, String programName, String description) {
        String username = jwtUtils.getUsernameFromJwtToken(jwtUtils.retrieveToken(token));
        User user = userService.getUserByUsername(username);
        Request request = Request.builder()
                .user(user)
                .status(RequestStatusEnum.CREATED)
                .priority(RequestPriorityEnum.DEFAULT)
                .paymentProgram(paymentProgramService.getPaymentProgramByName(programName))
                .description(description)
                .lastUpdate(LocalDateTime.now())
                .build();

        Request savedRequest = requestRepository.save(request);

        Notification notification = Notification.builder()
                .user(user)
                .request(savedRequest)
                .message("Заява була створена успішно.")
                .creationTime(LocalDateTime.now())
                .build();

        notificationService.send(notification);
        notificationService.save(notification);

        return savedRequest;
    }
}
