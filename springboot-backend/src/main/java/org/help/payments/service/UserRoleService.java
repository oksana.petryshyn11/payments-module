package org.help.payments.service;

import org.help.payments.model.UserRole;
import org.help.payments.model.UserRoleEnum;
import org.help.payments.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRoleService {

    @Autowired
    private UserRoleRepository userRoleRepository;

    public UserRole findByName(UserRoleEnum roleUser) {
        return userRoleRepository.findByName(roleUser)
                .orElseThrow(() -> new RuntimeException("Error: Role " + roleUser + " is not found."));
    }
}
