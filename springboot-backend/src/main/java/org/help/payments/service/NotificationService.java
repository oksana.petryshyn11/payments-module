package org.help.payments.service;

import org.help.payments.model.Notification;
import org.help.payments.notification.TelegramNotificationSender;
import org.help.payments.repository.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NotificationService {

    @Autowired
    private TelegramNotificationSender sender;

    @Autowired
    private NotificationRepository notificationRepository;

    public void send(Notification notification) {
        sender.send(notification);
    }

    public Notification save(Notification notification) {
        return notificationRepository.save(notification);
    }

}
