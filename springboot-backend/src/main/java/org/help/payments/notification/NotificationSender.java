package org.help.payments.notification;

import org.help.payments.model.Notification;

public interface NotificationSender {
    void send(Notification notification);
}
