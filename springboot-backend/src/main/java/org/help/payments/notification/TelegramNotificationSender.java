package org.help.payments.notification;

import org.help.payments.model.Notification;
import org.help.payments.model.Request;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Component
public class TelegramNotificationSender extends TelegramLongPollingBot implements NotificationSender {
    @Value("${telegram.bot.username}")
    private String username;
    @Value("${telegram.bot.token}")
    private String token;

    @Override
    public String getBotUsername() {
        return username;
    }

    @Override
    public String getBotToken() {
        return token;
    }

    @Override
    public void onUpdateReceived(Update update) {

    }

    @Override
    public void send(Notification notification) {
        SendMessage sm = new SendMessage();
        sm.setChatId(notification.getRequest().getUser().getTelegramId());

        Request request = notification.getRequest();
        sm.setText("Оновлення в заявці: #" + request.getId() + " - " + request.getDescription() + "\n" +
                notification.getMessage());

        try {
            execute(sm);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}