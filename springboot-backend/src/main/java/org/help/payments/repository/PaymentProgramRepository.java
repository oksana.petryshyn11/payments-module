package org.help.payments.repository;

import org.help.payments.model.PaymentProgram;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PaymentProgramRepository extends JpaRepository<PaymentProgram, Long> {

    Optional<PaymentProgram> findByName(String name);
}


