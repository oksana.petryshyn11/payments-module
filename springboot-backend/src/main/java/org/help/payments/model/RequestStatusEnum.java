package org.help.payments.model;


public enum RequestStatusEnum {
    CREATED, IN_REVIEW, APPROVED, DONE, CANCELLED
}
