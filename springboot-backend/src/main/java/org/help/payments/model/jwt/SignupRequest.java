package org.help.payments.model.jwt;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
public class SignupRequest {
    @NotBlank
    @Size(min = 3, max = 20)
    private String username;

    @NotBlank
    @Size(max = 50)
    @Email
    private String email;

    private Set<String> role;

    @NotBlank
    private String name;

    @NotBlank
    private String surname;

    private Date birthdate;

    @NotBlank
    private String phoneNumber;

    private String address;

    private long telegramId;

    @NotBlank
    private String passportNo;

    @NotBlank
    private String iban;

    @NotBlank
    @Size(min = 6, max = 40)
    private String password;
}