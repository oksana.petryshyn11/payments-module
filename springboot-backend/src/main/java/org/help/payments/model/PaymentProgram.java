package org.help.payments.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "payment_program")
public class PaymentProgram {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotBlank
    @Column(length = 30)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "amount")
    private Integer amount; // TODO: Change type

    @Column(name = "is_active")
    private Boolean isActive;
}