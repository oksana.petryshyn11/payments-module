package org.help.payments.model;

public enum UserRoleEnum {
    ROLE_ADMIN, ROLE_ACCOUNTANT, ROLE_SPECIALIST, ROLE_USER
}
