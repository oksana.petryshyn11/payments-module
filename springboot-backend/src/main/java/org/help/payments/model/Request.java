package org.help.payments.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
@Table(name = "request")
public class Request {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(cascade = {CascadeType.DETACH})
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(cascade = {CascadeType.DETACH})
    @NotNull
    @JoinColumn(name = "payment_program")
    private PaymentProgram paymentProgram;

    @Column(name = "description")
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private RequestStatusEnum status = RequestStatusEnum.CREATED;

    @Enumerated(EnumType.STRING)
    @Column(name = "priority")
    private RequestPriorityEnum priority = RequestPriorityEnum.DEFAULT;

    @Column(name = "last_update")
    private LocalDateTime lastUpdate;

}
