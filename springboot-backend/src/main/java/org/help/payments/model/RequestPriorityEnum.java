package org.help.payments.model;


public enum RequestPriorityEnum {
    LOW, DEFAULT, HIGH
}
