package org.help.payments.controller;

import org.help.payments.model.User;
import org.help.payments.model.UserRole;
import org.help.payments.model.UserRoleEnum;
import org.help.payments.model.jwt.JwtResponse;
import org.help.payments.model.jwt.LoginRequest;
import org.help.payments.model.jwt.MessageResponse;
import org.help.payments.model.jwt.SignupRequest;
import org.help.payments.security.JwtUtils;
import org.help.payments.security.UserDetailsImpl;
import org.help.payments.service.UserRoleService;
import org.help.payments.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private UserService userService;

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private JwtUtils jwtUtils;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .toList();

        return ResponseEntity.ok(new JwtResponse(roles,
                jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail()));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        if (userService.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (userService.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }

        // Create new user's account
        User user = User.builder()
                .name(signUpRequest.getName())
                .surname(signUpRequest.getSurname())
                .birthdate(signUpRequest.getBirthdate())
                .phoneNumber(signUpRequest.getPhoneNumber())
                .email(signUpRequest.getEmail())
                .address(signUpRequest.getAddress())
                .telegramId(signUpRequest.getTelegramId())
                .username(signUpRequest.getUsername())
                .password(encoder.encode(signUpRequest.getPassword()))
                .passportNo(signUpRequest.getPassportNo())
                .iban(signUpRequest.getIban())
                .build();

        Set<String> strRoles = signUpRequest.getRole();
        Set<UserRole> roles = new HashSet<>();

        if (strRoles == null) {
            UserRole userRole = userRoleService.findByName(UserRoleEnum.ROLE_USER);
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin" -> {
                        UserRole adminRole = userRoleService.findByName(UserRoleEnum.ROLE_ADMIN);
                        roles.add(adminRole);
                    }
                    case "specialist" -> {
                        UserRole modRole = userRoleService.findByName(UserRoleEnum.ROLE_SPECIALIST);
                        roles.add(modRole);
                    }
                    case "accountant" -> {
                        UserRole accRole = userRoleService.findByName(UserRoleEnum.ROLE_ACCOUNTANT);
                        roles.add(accRole);
                    }
                    default -> {
                        UserRole userRole = userRoleService.findByName(UserRoleEnum.ROLE_USER);
                        roles.add(userRole);
                    }
                }
            });
        }

        user.setRoles(roles);
        userService.save(user);

        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }
}