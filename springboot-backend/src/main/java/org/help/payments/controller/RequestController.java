package org.help.payments.controller;

import org.help.payments.model.Request;
import org.help.payments.model.RequestDetails;
import org.help.payments.model.RequestStatusEnum;
import org.help.payments.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/requests")
public class RequestController {

    @Autowired
    private RequestService requestService;

    @GetMapping
    public List<Request> getRequests(@RequestHeader(HttpHeaders.AUTHORIZATION) String token) {
        return requestService.getRequests(token);
    }

    @PostMapping
    @PreAuthorize("hasRole('USER')")
    public Request createRequest(@RequestHeader(HttpHeaders.AUTHORIZATION) String token, @RequestBody RequestDetails requestDetails) {
        return requestService.createRequest(token, requestDetails.getProgramName(), requestDetails.getDescription());
    }

    @GetMapping("{id}")
    public ResponseEntity<Request> getRequestById(@PathVariable long id) {
        Request request = requestService.getRequestById(id);
        return ResponseEntity.ok(request);
    }

    @PutMapping("/change_status/{id}")
    @PreAuthorize("hasRole('SPECIALIST') or hasRole('ACCOUNTANT')")
    public ResponseEntity<Request> updateRequestStatus(@PathVariable long id, @RequestParam String status) {
        Request request = requestService.updateRequestWithStatus(id, status);
        return ResponseEntity.ok(request);
    }

    @PutMapping("/change_priority/{id}")
    @PreAuthorize("hasRole('SPECIALIST')")
    public ResponseEntity<Request> updateRequestPriority(@PathVariable long id, @RequestParam String priority) {
        Request request = requestService.updateRequestWithPriority(id, priority);
        return ResponseEntity.ok(request);
    }

    @PutMapping("/delete/{id}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<Request> deleteRequest(@PathVariable long id) {
        Request request = requestService.updateRequestWithStatus(id, RequestStatusEnum.CANCELLED.name());
        return ResponseEntity.ok(request);
    }
}
