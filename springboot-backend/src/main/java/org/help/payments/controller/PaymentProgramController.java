package org.help.payments.controller;

import org.help.payments.model.PaymentProgram;
import org.help.payments.service.PaymentProgramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/payment_program")
public class PaymentProgramController {

    @Autowired
    private PaymentProgramService paymentProgramService;

    @GetMapping
    public List<PaymentProgram> getPaymentPrograms() {
        return paymentProgramService.getAll();
    }

    @GetMapping("{id}")
    public PaymentProgram getPaymentProgramById(@PathVariable long id) {
        return paymentProgramService.getPaymentProgramById(id);
    }

    @PostMapping
    @PreAuthorize("hasRole('SPECIALIST') or hasRole('ADMIN')")
    public PaymentProgram createPaymentProgram(@RequestBody PaymentProgram paymentProgram) {
        return paymentProgramService.add(paymentProgram);
    }

    @PutMapping("{id}")
    @PreAuthorize("hasRole('SPECIALIST') or hasRole('ADMIN')")
    public PaymentProgram updatePaymentProgram(@PathVariable long id, @RequestBody PaymentProgram paymentProgram) {
        return paymentProgramService.update(id, paymentProgram);
    }
}
