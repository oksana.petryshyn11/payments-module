﻿INSERT INTO role (name) VALUES ('ROLE_ADMIN'), ('ROLE_USER'), ('ROLE_SPECIALIST'), ('ROLE_ACCOUNTANT');

INSERT INTO payment_program
           (amount
           ,description
           ,name)
     VALUES
           (1000,'Тисяча гривень від президента', 'Тисяча Зеленського'),
		   (10000,'Допомога малому бізнесу України', 'Малий бізнес'),
		   (3000,'Одноразова допомога внутрішньо переміщеним особам', 'ВПО');


